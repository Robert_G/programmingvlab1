﻿using System;

namespace stacklib
{
    public class MyStack : I510Stack
    {
        private char[] arr;
        public MyStack(int fixedSize){
            arr = new char[fixedSize];
        }

        /// <summary>
        /// Adds a new character to the stack.
        /// </summary>
        /// <param name="character">Specifies the character to add to the stack.</param>
        public void Push(char character){
            int size = Length();
            if(size >= arr.Length){
                throw new OutOfMemoryException();
            }
            arr[size] = character;

        }
        /// <summary>
        /// Removes the top most character from the stack and returns it to the caller. 
        /// The removed item must be replaced with the null terminiating string \0.
        /// </summary>
        /// <returns>The character poped off the stack</returns>
        public char Pop(){
            if(Length() == 0){
                throw new IndexOutOfRangeException();
            }
            // Last in, first out
            char top = arr[Length() - 1];
            arr[Length() - 1] = '\0';

            //first in, first out
            // char top = arr[0];
            // for (int i = 0; i < Length() - 1; i++){
            //     arr[i] = arr[i + 1];
            // }
            // arr[Length() - 1] = '\0';
            return top;
        }

        /// <summary>
        /// Returns the current top most character but does not remove it from the stack.
        /// </summary>
        /// <returns>The top most character</returns>
        public char Peek(){
            if(Length() == 0){
                return '\0';
            }
            return arr[Length() - 1];
        }

        /// <summary>
        /// Returns the current length of the stack.
        /// </summary>
        /// <returns>Stack length</returns>
        public int Length(){
            int count = 0;
            foreach(char i in arr){
                if (i != '\0'){
                    count++;
                }
            }
            return count;
        }
    }
}
