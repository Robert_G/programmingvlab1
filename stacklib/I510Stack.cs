namespace stacklib
{
    public interface I510Stack
    {
        /// <summary>
        /// Adds a new character to the stack.
        /// </summary>
        /// <param name="character">Specifies the character to add to the stack.</param>
        void Push(char character);
        
        /// <summary>
        /// Removes the top most character from the stack and returns it to the caller. 
        /// The removed item must be replaced with the null terminiating string \0.
        /// </summary>
        /// <returns>The character poped off the stack</returns>
        char Pop();

        /// <summary>
        /// Returns the current top most character but does not remove it from the stack.
        /// </summary>
        /// <returns>The top most character</returns>
        char Peek();

        /// <summary>
        /// Returns the current length of the stack.
        /// </summary>
        /// <returns>Stack length</returns>
        int Length();
    }
}