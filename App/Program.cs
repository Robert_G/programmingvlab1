﻿using System;
using stacklib;
namespace App

{
    class Program
    {

        public static bool ValidBrackets(String brackets)
        {
            char[] split = brackets.ToCharArray();
            I510Stack myStackBrackets = new MyStack(split.Length);
            foreach (char bracket in split)
            {
                if (bracket.Equals('[') || bracket.Equals('(') || bracket.Equals('{'))
                {
                    myStackBrackets.Push(bracket);
                }
                else if (bracket.Equals(']') || bracket.Equals(')') || bracket.Equals('}'))
                {
                    if (myStackBrackets.Length() == 0)
                    {
                        return false;
                    }
                    else if (!isBracketsMatch(myStackBrackets.Pop(), bracket))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            if (myStackBrackets.Length() == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool isBracketsMatch(char char1, char char2)
        {
            if (char1.Equals('[') && char2.Equals(']'))
            {
                return true;
            }
            else if (char1.Equals('(') && char2.Equals(')'))
            {
                return true;
            }
            else if (char1.Equals('{') && char2.Equals('}'))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static void Main(string[] args)
        {
            String correctStack = "{{}}()[()]";
            String incorrectStack = "(()";
            Console.WriteLine("Is first stack correct?: " + ValidBrackets(correctStack));
            Console.WriteLine("Is second stack correct?: " + ValidBrackets(incorrectStack));
        }
    }
}
